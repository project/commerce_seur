<?php

/**
 * @file
 * Handles XML-related stuff for Commerce seur module.
 */
/**
 * This builds the XML to submit to seur for rates.
 */
if (!function_exists("commerce_seur_build_rate_request")) {

  function commerce_seur_build_rate_request($order) {
    // Build configuration
    $shipping_details = field_get_items('commerce_order', $order, 'commerce_customer_shipping');
    $shipping_profile = commerce_customer_profile_load($shipping_details[0]['profile_id']);

    if ($shipping_profile) {
      $address_info = field_get_items('commerce_customer_profile', $shipping_profile, 'commerce_customer_address');
    }
    else {
      $address_info = array(
        0 => array(
          'locality' => 'Madrid',
          'postal_code' => '28080',
        ),
      );
    }

    $dimension_table = 'field_data_field_' . variable_get('commerce_seur_field_dimensions', '');
    $weight_table = 'field_data_field_' . variable_get('commerce_seur_field_weight', '');

    $width_field = getWidth();
    $height_field = getHeight();
    $length_field = getLength();
    $weight_field = getWeight();

    $response = array();

    $Bultos = 1;
    $Peso = 0;
    $PesoVolumen = 0;

    foreach (_commerce_seur_get_cart_products($order) as $lineItem) {
      $q = $lineItem['q'];
      do {
        $info_medidas = get_dimensions($dimension_table, $lineItem['product_id']);
        $info_peso = get_weight($weight_table, $lineItem['product_id']);

        $PesoVolumen += calcula_pesoVolumen($info_medidas);
        $Peso += $info_peso[0]->$weight_field;

        $longitud_maxima += ($info_medidas[0]->$width_field * 2) + ($info_medidas[0]->$height_field * 2) + $info_medidas[0]->$length_field;

        if ($longitud_maxima > 300) {
          $Bultos++;
          $longitud_maxima = 0;
        }
        $q--;
      }
      while ($q > 0);
    }

    if (!empty($PesoVolumen) && (!empty($Peso))) {
      $xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
      $xml .= "<CAMPOS>";
      $xml .= "<USUARIO>" . variable_get('commerce_seur_user_id', '') . "</USUARIO>";
      $xml .= "<PASSWORD>" . variable_get('commerce_seur_password', '') . "</PASSWORD>";
      $xml .= "<NOM_POBLA_DEST>" . $address_info[0]['locality'] . "</NOM_POBLA_DEST>";
      $xml .= "<CODIGO_POSTAL_DEST>" . $address_info[0]['postal_code'] . "</CODIGO_POSTAL_DEST>";
      $xml .= "<CodContableRemitente>" . variable_get('commerce_seur_codcontableremitente', '') . "</CodContableRemitente>";
      $xml .= "<Peso>" . $Peso . "</Peso>";
      $xml .= "<Bultos>" . $Bultos . "</Bultos>";
      $xml .= "<PesoVolumen>" . $PesoVolumen . "</PesoVolumen>";
      $xml .= "</CAMPOS>";

      return $xml; /* ->saveXML(); */
    }
  }

}

if (!function_exists("calcula_pesoVolumen")) {

  function calcula_pesoVolumen($info_medidas) {
    return ($info_medidas[0]->field_dimensiones_env_o_length * $info_medidas[0]->field_dimensiones_env_o_height * $info_medidas[0]->field_dimensiones_env_o_width ) / 6000;
  }

}

/*
 * Function to calcule limits
 */
if (!function_exists("calc_group_limit")) {

  function calc_group_limit($info_medidas, $info_peso) {
    $width_field = getWidth();
    $height_field = getHeight();
    $length_field = getLength();
    $weight_field = getWeight();

    $itemVolume = (($info_medidas[0]->$width_field * 2) + ($info_medidas[0]->$height_field * 2) + $info_medidas[0]->$length_field);

    $vlimit = 175 / $itemVolume;
    $wlimit = 20 / $info_peso[0]->$weight_field;

    if ($vlimit < $wlimit) {
      return $vlimit;
    }
    else {
      return $wlimit;
    }
  }

}

if (!function_exists("calcula_medidas")) {

  function calcula_medidas($info_medidas, $res) {
    $width_field = getWidth();
    $height_field = getHeight();
    $length_field = getLength();
    $weight_field = getWeight();

    if (empty($res)) {
      $res[$height_field] = 0;
      $res[$width_field] = 0;
      $res[$length_field] = 0;
    }

    $width = $info_medidas[0]->$width_field + $pre[$width_field];
    $height = $info_medidas[0]->$height_field + $pre[$height_field];
    $length = $info_medidas[0]->$length_field + $pre[$length_field];

    if (($width < $height) && ( $width < $length)) {
      $info_medidas[0]->$width_field = $width;
      if ($info_medidas[0]->$height_field < $res[$height_field]) {
        $info_medidas[0]->$height_field = $res[$height_field];
      }
      if ($info_medidas[0]->$length_field < $res[$length_field]) {
        $info_medidas[0]->$length_field = $res[$length_field];
      }
    }

    if (($height < $width) && ( $height < $length)) {
      $info_medidas[0]->$height_field = $height;
      if ($info_medidas[0]->$width_field < $res[$width_field]) {
        $info_medidas[0]->$width_field = $res[$width_field];
      }
      if ($info_medidas[0]->$length_field < $res[$length_field]) {
        $info_medidas[0]->$length_field = $res[$length_field];
      }
    }

    if (($length < $height) && ( $length < $width)) {
      $info_medidas[0]->$length_field = $length;
      if ($info_medidas[0]->$height_field < $res[$height_field]) {
        $info_medidas[0]->$height_field = $res[$height_field];
      }
      if ($info_medidas[0]->$width_field < $res[$width_field]) {
        $info_medidas[0]->$width_field = $res[$width_field];
      }
    }

    return $info_medidas;
  }

}

// Get product dimension data
if (!function_exists("get_dimensions")) {

  function get_dimensions($dimension_table, $product_id) {
    $query = db_select($dimension_table, 'ct');
    $query->fields('ct', array(
      'entity_id',
      getLength(),
      getWidth(),
      getHeight(),
    ));

    $query->condition('ct.entity_id', $product_id);
    return $query->execute()->fetchAll();
  }

}

// Get product weight data
if (!function_exists("get_weight")) {

  function get_weight($weight_table, $product_id) {

    $query = db_select($weight_table, 'ct');
    $query->fields('ct', array(
      'entity_id',
      getWeight(),
    ));

    $query->condition('ct.entity_id', $product_id);

    return $query->execute()->fetchAll();
  }

}

/**
 * Submits an API request to seur.
 *
 * @param string $data
 *   An XML string to submit to seur.
 */
if (!function_exists("commerce_seur_api_request")) {

  function commerce_seur_api_request($data) {
    require_once('lib/nusoap.php');

    if (empty($data)) {
      return FALSE;
    }

    $url = variable_get('commerce_seur_post_url', '');
    $request = new nusoap_client($url, true);

    $err = $request->getError();

    if ($err) {
      echo '<p><b>Error: ' . $err . '</b></p>';
    }

    $args = array(
      "in0" => $data
    );

    $result = $request->call('tarificacionPrivadaStr', $args);

    // If we received data back from the server...
    if (!empty($result)) {
      return $result;
    }
    else {
      return FALSE;
    }
  }

}

if (!function_exists("getWidth")) {

  function getWidth() {
    return 'field_' . variable_get('commerce_seur_field_dimensions', '') . '_width';
  }

}

if (!function_exists("getHeight")) {

  function getHeight() {
    return 'field_' . variable_get('commerce_seur_field_dimensions', '') . '_height';
  }

}

if (!function_exists("getLength")) {

  function getLength() {
    return 'field_' . variable_get('commerce_seur_field_dimensions', '') . '_length';
  }

}

if (!function_exists("getWeight")) {

  function getWeight() {
    return 'field_' . variable_get('commerce_seur_field_weight', '') . '_weight';
  }

}
