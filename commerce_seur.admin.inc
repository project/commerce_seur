<?php

/**
 * @file
 * seur administration menu items.
 */

/**
 * seur Quote settings.
 *
 * Record seur account information neccessary to use the service.
 * Configure which seur services are quoted to customers.
 *
 * @ingroup forms
 * @see uc_admin_settings_validate()
 */
function commerce_seur_settings_form() {
  $form = array();

  $form['commerce_seur_post_url'] = array(
    '#type' => 'textfield',
    '#title' => t('SEUR POST URL'),
    '#description' => t('The URL that we post our request to.'),
    '#default_value' => variable_get('commerce_seur_post_url', ''),
    '#required' => TRUE,
  );

  $form['commerce_seur_codcontableremitente'] = array(
    '#type' => 'textfield',
    '#title' => t('SEUR Cod. Contable Remitente'),
    '#description' => t('NNNNN-NN'),
    '#default_value' => variable_get('commerce_seur_codcontableremitente', ''),
    '#required' => TRUE,
  );

  $form['commerce_seur_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('SEUR User ID'),
    '#default_value' => variable_get('commerce_seur_user_id', ''),
    '#required' => TRUE,
  );

  $form['commerce_seur_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('commerce_seur_password', ''),
  );

  $form['commerce_seur_field_dimensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Campo dimensiones del producto'),
    '#default_value' => variable_get('commerce_seur_field_dimensions', ''),
  );

  $form['commerce_seur_field_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Campo Peso del Producto'),
    '#default_value' => variable_get('commerce_seur_field_weight', ''),
  );

  return system_settings_form($form);
}

/**
 * Validation handler for commerce_seur_settings_form.
 *
 * Require password only if it hasn't been set.
 *
 * @see commerce_seur_settings_form()
 */
function commerce_seur_settings_form_validate($form, &$form_state) {
  $old_password = variable_get('commerce_seur_password', '');
  if (!$form_state['values']['commerce_seur_password']) {
    if ($old_password) {
      form_set_value($form['commerce_seur_password'], $old_password, $form_state);
    }
    else {
      form_set_error('commerce_seur_password', t('Password field is required.'));
    }
  }
}
